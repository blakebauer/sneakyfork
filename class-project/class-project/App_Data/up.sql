﻿CREATE TABLE Athletes
(	
	AthleteID		INT				IDENTITY(1, 1)			PRIMARY KEY
	,FirstName		VARCHAR(50)		NOT NULL
	,LastName		VARCHAR(100)	NOT NULL
	,DOB			DATETIME		NOT NULL
	,Weight			FLOAT			NULL
	,Height			FLOAT			NULL
	,Grade			INT				NOT NULL
	,Team			VARCHAR(50)		NOT NULL
);
GO

CREATE TABLE Guardians
(
	GID				INT				IDENTITY(200000, 1)		PRIMARY KEY
	,FirstName		VARCHAR(50)		NOT NULL
	,LastName		VARCHAR(100)	NOT NULL
	,AccessLevel	INT				NOT NULL
);
GO

CREATE TABLE AthleteGuardians
(
	AthleteID		INT				NOT NULL
	,GID			INT				NOT NULL
	,CONSTRAINT		AtGrdPK			PRIMARY KEY(AthleteID, GID)
	,CONSTRAINT		AthFK			FOREIGN KEY(AthleteID) REFERENCES dbo.Athletes(AthleteID)
									ON UPDATE NO ACTION ON DELETE CASCADE
	,CONSTRAINT		GrdFK			FOREIGN KEY(GID)		REFERENCES dbo.Guardians(GID)
									ON UPDATE NO ACTION ON DELETE CASCADE
);
GO

CREATE TABLE RunFiles
(
	FileID			INT				IDENTITY(1000000, 1)	PRIMARY KEY
	,AthleteID		INT				NOT NULL
	,FitFilePath	VARCHAR(300)	NOT NULL
	,CONSTRAINT		AthFileFK		FOREIGN KEY(AthleteID) REFERENCES dbo.Athletes(AthleteID)
									ON UPDATE NO ACTION ON DELETE CASCADE
);
GO

CREATE TABLE Runs
(
	RunID			INT				NOT NULL
	,AthleteID		INT				NOT NULL
	,FileID			INT				NOT NULL
	,Distance		FLOAT			NOT NULL
	,Time			DATE			NOT NULL
	,AvgHR			FLOAT			NOT NULL
	,MaxHR			FLOAT			NOT NULL
	,CONSTRAINT		RunPK			PRIMARY KEY(RunID, AthleteID, FileID)
	,CONSTRAINT		FileFK			FOREIGN KEY(FileID)	REFERENCES dbo.RunFiles(FileID)
									ON UPDATE NO ACTION ON DELETE NO ACTION
	,CONSTRAINT		AthRunFK		FOREIGN KEY(AthleteID)	REFERENCES dbo.Athletes(AthleteID)
									ON UPDATE NO ACTION ON DELETE CASCADE
);
GO

INSERT INTO dbo.Athletes
(	
	FirstName
	,LastName
	,DOB
	,Weight
	,Height
	,Grade	
	,Team
) VALUES
	('Jared',		'Brooks',	GETDATE(), 200, 5.6,	12,	'A')
	,('Sonja',		'Castillo', GETDATE(), 122, 4.6,	12,	'A')
	,('Forrest',	'Burgess',	GETDATE(), 333, 7.6,	12,	'A')
	,('Darin',		'Lindsey',	GETDATE(), 155, 5.8,	12,	'B')
	,('Kristina',	'Green',	GETDATE(), 122, 6,		12,	'C')
	,('Cesar',		'Nguyen',	GETDATE(), 166, 6.6,	11,	'C')
	,('Betsy',		'Peters',	GETDATE(), 126, 5.0,	11,	'C')
	,('Joanna',		'Yates',	GETDATE(), 222, 5.9,	11,	'B')
	,('Joseph',		'Palmer',	GETDATE(), 321, 5.10,	11,	'B')
	,('Alvin',		'Mckinney', GETDATE(), 133, 5.11,	11,	'B');
GO

INSERT INTO dbo.Guardians
(
	FirstName
	,LastName
	,AccessLevel
) VALUES
	('Pat',			'Grant',	1)
	,('Lynda',		'Yates',	2)
	,('Marlene',	'Drake',	2)
	,('Shelley',	'Payne',	2)
	,('Tasha',		'Murray',	2)
	,('Sherri',		'Hart',		2)
	,('Barry',		'Kelley',	2)
	,('Chelsea',	'Powell',	2)
	,('Michele',	'Luna',		2)
	,('Joe',		'Johnson',	2)
	,('Bob',		'Doel',		2);
GO

INSERT INTO dbo.AthleteGuardians
(
	AthleteID
	,GID
) VALUES
	(1,		200000)
	,(2,	200000)
	,(3,	200000)
	,(4,	200000)
	,(5,	200000)
	,(6,	200000)
	,(7,	200000)
	,(8,	200000)
	,(9,	200000)
	,(10,	200000)
	,(1,	200001)
	,(2,	200002)
	,(3,	200003)
	,(4,	200004)
	,(5,	200005)
	,(6,	200006)
	,(7,	200007)
	,(8,	200008)
	,(9,	200009)
	,(10,	200010);
GO

INSERT INTO dbo.RunFiles
(
	AthleteID
	,FitFilePath
) VALUES
	(1,		'C:\Users\User\Documents\02dktjs0fsg.fit')
	,(3,	'C:\Users\User\Documents\34ysdt4h.fit')
	,(5,	'C:\Users\User\Documents\sdh34t65s.fit')
	,(2,	'C:\Users\User\Documents\hstje4e5.fit')
	,(4,	'C:\Users\User\Documents\dfgjdetrje.fit')
	,(9,	'C:\Users\User\Documents\df5455433677.fit')
	,(10,	'C:\Users\User\Documents\455545gfh4h.fit')
	,(6,	'C:\Users\User\Documents\dejfth45y.fit')
	,(7,	'C:\Users\User\Documents\dfjer6y445.fit')
	,(8,	'C:\Users\User\Documents\45ydjfthy4.fit');
GO

INSERT INTO dbo.Runs
(
	RunID
	,AthleteID
	,FileID
	,Distance
	,Time	
	,AvgHR	
	,MaxHR	
) VALUES
	(500010,	1,	1000000,	4.3,	GETDATE(),	70,	99)
	,(500001,	3,	1000001,	3.3,	GETDATE(),	76,	95)
	,(500002,	5,	1000002,	4.5,	GETDATE(),	66,	99)
	,(500003,	4,	1000004,	4.5,	GETDATE(),	77,	96)
	,(500004,	2,	1000003,	5.3,	GETDATE(),	79,	94)
	,(500005,	6,	1000007,	6.5,	GETDATE(),	83,	100)
	,(500006,	10, 1000006,	3.8,	GETDATE(),	62,	89)
	,(500007,	9,	1000005,	5.2,	GETDATE(),	67,	93)
	,(500008,	7,	1000008,	5,		GETDATE(),	76,	94)
	,(500009,	8,	1000009,	4.3,	GETDATE(),	90,	97);
GO