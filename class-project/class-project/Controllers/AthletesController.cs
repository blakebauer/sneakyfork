﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using class_project.Models;

namespace class_project.Controllers
{
    public class AthletesController : Controller
    {
        private ClassProjectContext db = new ClassProjectContext();

        // GET: Athletes
        public ActionResult Index()
        {
            return View(db.Athletes.ToList());
        }


        public ActionResult Teams(string id)
        {
            if (id == "")
                // if doens't exists 
                return RedirectToAction("Index");
            var running = db.Runs;

            var teamAthletes = db.Athletes.Where(x => x.Team.StartsWith(id) && x.Team.EndsWith(id));

            ViewBag.heart = db.Runs.Where(x => x.Athlete.Team.StartsWith(id) && x.Athlete.Team.EndsWith(id))
                .Select(s => new { s.Athlete.LastName, s.AvgHR }).AsEnumerable().Select(c => new Tuple<string, double>(c.LastName, c.AvgHR)).ToList();
            return View(teamAthletes);
        }

        public ActionResult Date(int? id)
        {
            if (id == null)
                // if doens't exists 
                return RedirectToAction("Index");
            var bday = db.Athletes.Where(x => x.DOB.Year == id);

            ViewBag.heart = db.Runs.Where(x => x.Athlete.DOB.Year == id).Select(s => new { s.Athlete.LastName, s.AvgHR })
                .AsEnumerable().Select(c => new Tuple<string, double>(c.LastName, c.AvgHR)).ToList();

            return View(bday.ToList());
        }

        // GET: Athletes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Athlete athlete = db.Athletes.Find(id);
            if (athlete == null)
            {
                return HttpNotFound();
            }
            return View(new AthleteRun(athlete));
        }

        // GET: Athletes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Athletes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AthleteID,FirstName,LastName,DOB,Weight,Height,Grade,Team")] Athlete athlete)
        {
            if (ModelState.IsValid)
            {
                db.Athletes.Add(athlete);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(athlete);
        }

        // GET: Athletes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Athlete athlete = db.Athletes.Find(id);
            if (athlete == null)
            {
                return HttpNotFound();
            }
            return View(athlete);
        }

        // POST: Athletes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AthleteID,FirstName,LastName,DOB,Weight,Height,Grade,Team")] Athlete athlete)
        {
            if (ModelState.IsValid)
            {
                db.Entry(athlete).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(athlete);
        }

        // GET: Athletes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Athlete athlete = db.Athletes.Find(id);
            if (athlete == null)
            {
                return HttpNotFound();
            }
            return View(athlete);
        }

        // POST: Athletes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Athlete athlete = db.Athletes.Find(id);
            db.Athletes.Remove(athlete);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
