﻿-- Exported from QuickDBD: https://www.quickdatatabasediagrams.com/
-- Link to schema: https://app.quickdatabasediagrams.com/#/schema/GP4uKzQva0GgrxkrgcNNtg
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.


SET XACT_ABORT ON

BEGIN TRANSACTION QUICKDBD

CREATE TABLE [Tasks] (
    [TaskID] INT IDENTITY(1,1) NOT NULL ,
    [TaskName] VARCHAR(40)  NOT NULL ,
    CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED (
        [TaskID] ASC
    )
)

CREATE TABLE [Dates] (
    [DateID] INT IDENTITY(1,1) NOT NULL ,
    [StartDate] DATETIME  NOT NULL ,
    [EndDate] DATETIME  NOT NULL ,
    [StartTime] DATETIME  NOT NULL ,
    [EneTime] DATETIME  NOT NULL ,
    [Reacurring] BIT  NOT NULL ,
    [TaskID] INT  NOT NULL ,
    CONSTRAINT [PK_Dates] PRIMARY KEY CLUSTERED (
        [DateID] ASC
    )
)

CREATE TABLE [TaskActions] (
    [TActionID] INT IDENTITY(1,1) NOT NULL ,
    [TActionName] VARCHAR(50)  NOT NULL ,
    [TaskID] INT  NOT NULL ,
    CONSTRAINT [PK_TaskActions] PRIMARY KEY CLUSTERED (
        [TActionID] ASC
    )
)

CREATE TABLE [TaskParameters] (
    [TActionID] INT  NOT NULL ,
    [ActionID] INT  NOT NULL ,
    [TParameterOne] VARCHAR(MAX)  NOT NULL ,
    [TParameterTwo] VARCHAR(MAX)  NOT NULL ,
    [TPBit] BIT  NOT NULL ,
    CONSTRAINT [PK_TaskParameters] PRIMARY KEY CLUSTERED (
        [TActionID] ASC,[ActionID] ASC
    )
)

CREATE TABLE [TaskUsers] (
    [ContactID] INT  NOT NULL ,
    [TActionID] INT  NOT NULL ,
    CONSTRAINT [PK_TaskUsers] PRIMARY KEY CLUSTERED (
        [ContactID] ASC,[TActionID] ASC
    )
)

CREATE TABLE [Actions] (
    [ActionID] INT IDENTITY(1,1) NOT NULL ,
    [ActionName] VARCHAR(50)  NOT NULL ,
    [ActionDesc] VARCAHR(400)  NOT NULL ,
    [ActionMethod] VARCHAR(50)  NOT NULL ,
    CONSTRAINT [PK_Actions] PRIMARY KEY CLUSTERED (
        [ActionID] ASC
    )
)

CREATE TABLE [Contacts] (
    [ContactID] INT IDENTITY(1,1) NOT NULL ,
    [FirstName] VARCHAR(50)  NOT NULL ,
    [LastName] VARCHAR(50)  NOT NULL ,
    [Email] VARCHAR(150)  NULL ,
    [Phone] VARCHAR(30)  NOT NULL ,
    [Photo] IMAGE  NULL ,
    CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED (
        [ContactID] ASC
    )
)

ALTER TABLE [Tasks] WITH CHECK ADD CONSTRAINT [FK_Tasks_TaskID] FOREIGN KEY([TaskID])
REFERENCES [TaskActions] ([TaskID])

ALTER TABLE [Tasks] CHECK CONSTRAINT [FK_Tasks_TaskID]

ALTER TABLE [Dates] WITH CHECK ADD CONSTRAINT [FK_Dates_TaskID] FOREIGN KEY([TaskID])
REFERENCES [Tasks] ([TaskID])

ALTER TABLE [Dates] CHECK CONSTRAINT [FK_Dates_TaskID]

ALTER TABLE [TaskParameters] WITH CHECK ADD CONSTRAINT [FK_TaskParameters_TActionID] FOREIGN KEY([TActionID])
REFERENCES [TaskActions] ([TActionID])

ALTER TABLE [TaskParameters] CHECK CONSTRAINT [FK_TaskParameters_TActionID]

ALTER TABLE [TaskParameters] WITH CHECK ADD CONSTRAINT [FK_TaskParameters_ActionID] FOREIGN KEY([ActionID])
REFERENCES [Actions] ([ActionID])

ALTER TABLE [TaskParameters] CHECK CONSTRAINT [FK_TaskParameters_ActionID]

ALTER TABLE [TaskUsers] WITH CHECK ADD CONSTRAINT [FK_TaskUsers_ContactID] FOREIGN KEY([ContactID])
REFERENCES [Contacts] ([ContactID])

ALTER TABLE [TaskUsers] CHECK CONSTRAINT [FK_TaskUsers_ContactID]

ALTER TABLE [TaskUsers] WITH CHECK ADD CONSTRAINT [FK_TaskUsers_TActionID] FOREIGN KEY([TActionID])
REFERENCES [TaskActions] ([TActionID])

ALTER TABLE [TaskUsers] CHECK CONSTRAINT [FK_TaskUsers_TActionID]

COMMIT TRANSACTION QUICKDBD