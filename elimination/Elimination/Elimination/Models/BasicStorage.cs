﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Elimination.Models
{
    public class BasicStorage
    {
        private string blobConnect;
        private CloudStorageAccount storageAccount;
        private CloudBlobClient cloudBlobClient;

        public BasicStorage()
        {
            initialize();
        }

        /// <summary>
        /// Returns the name of the blob storage database
        /// </summary>
        public static string BlobStorageName
        {
            get { return WebConfigurationManager.AppSettings["BlobStorageName"]; }
        }

        /// <summary>
        /// Returns a Blob Uri that you can use to call ToString() on and have a url pointing to the 
        /// blob image
        /// </summary>
        /// <param name="blobContainer">Is the name of the blob container you are accessing.
        /// the blob name.</param>
        /// <returns>Returns a <seealso cref="Uri"/> object that with the <seealso cref="IListBlobItem"/> Url.</returns>
        public Uri GetBlob(string blobContainer = "default")
        {
            CloudBlobContainer cloudContainer = cloudBlobClient.GetContainerReference(blobContainer);

            IListBlobItem item = cloudContainer.ListBlobs(null, true).FirstOrDefault();

            return item.Uri;
        }

        /// <summary>
        /// Store a Blob to the Blob database
        /// </summary>
        /// <param name="blobContainer">Must start with a number or letter and then it could be numbers and characters
        /// and they can be separated by "-" dashes. name length must be between 3 an 63. If these rules aren't followed,
        /// a 404 error will be thrown.</param>
        /// <param name="blobName">The name of the blob to be stored. Please provide the file extension name.</param>
        /// <param name="filePath">The file path of the image or text file.</param>
        /// <returns></returns>
        public async Task<bool> StoreBlob(string blobContainer, string blobName, string filePath)
        {
            string validName = "^(?![0-9]+$)(?!-)[a-z0-9-]{3,63}(?<!-)";

            if (!Regex.IsMatch(blobContainer, validName))
                return false;

            // Retrieve a reference to a container.
            CloudBlobContainer cloudContainer = cloudBlobClient.GetContainerReference(blobContainer);

            // Create the container if it doesn't already exist.
            cloudContainer.CreateIfNotExists();

            // Set the permissions so the blobs are public. 
            BlobContainerPermissions permissions = new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            };

            await cloudContainer.SetPermissionsAsync(permissions);

            CloudBlockBlob blockBlob = cloudContainer.GetBlockBlobReference(blobName);
            await blockBlob.UploadFromFileAsync(filePath);

            return true;
        }

        /// <summary>
        /// Returns a formatted blob name
        /// </summary>
        /// <param name="extension">Then file extension to be used.</param>
        /// <param name="rvm">An optional <seealso cref="RegisterViewModel"/> that is used to build a Blob name.</param>
        /// <param name="player">An optional <seealso cref="Player"/> that is used to build a Blob name.</param>
        /// <returns>An acceptable blob name as a string</returns>
        public static string GetBlobName(string extension, RegisterViewModel rvm = null, Player player = null)
        {
            if (rvm != null)
                return $"{rvm.FirstName.ToLower()}{rvm.LastName.ToLower()}{extension}".Replace(" ", "");
            else if (player != null)
                return $"{player.FirstName.ToLower()}{player.LastName.ToLower()}{extension}".Replace(" ", "");
            else
                return $"default{extension}".Replace(" ", "");
        }

        /// <summary>
        /// Returns a formatted <seealso cref="CloudBlobContainer"/> name
        /// </summary>
        /// <param name="rvm">An optional <seealso cref="RegisterViewModel"/> 
        /// that is used to build a <seealso cref="CloudBlobContainer"/> name.</param>
        /// <param name="player">An optional <seealso cref="Player"/> that is used 
        /// to build a <seealso cref="CloudBlobContainer"/> name.</param>
        /// <returns>An acceptable <seealso cref="CloudBlobContainer"/> name as a string</returns>
        public static string GetContainerName(RegisterViewModel rvm = null, Player player = null)
        {
            if (rvm != null)
                return rvm.UserName.ToLower().Replace(" ", "");
            else if (player != null)
                return player.UserName.ToLower().Replace(" ", "");
            else
                return "default";
        }

        private void initialize()
        {
            try
            {
                blobConnect = ConfigurationManager.ConnectionStrings["EliminationPhotoBlob"].ConnectionString;
                storageAccount = CloudStorageAccount.Parse(blobConnect);
                cloudBlobClient = storageAccount.CreateCloudBlobClient();
            }
            catch (Exception e) { Debug.WriteLine(e.Message); }
        }
    }
}