/*
 * Created by Rajan Tawate.
 * User: Owner
 * Date: 9/3/2006
 * Time: 8:00 PM
 * 

 */

using System;
using System.Drawing;
using System.IO;
using System.Collections;
using System.Net;

namespace Elimination.Models
{
    /// <summary>
    /// Description of ImageConverter.
    /// </summary>
    public class ImageConverter
    {
        public ImageConverter()
        {
        }

        /// <summary>
        /// A <seealso cref="System.Drawing.Image"/> to be converted into a byte array.
        /// </summary>
        /// <param name="imageIn">The <seealso cref="System.Drawing.Image"/> to be converted into a byte array.</param>
        /// <returns>A <seealso cref="byte[]"/> that was converted from <paramref name="imageIn"/>.</returns>
        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        /// <summary>
        /// Converts an imgage array into a <seealso cref="System.Drawing.Image"/>.
        /// </summary>
        /// <param name="byteArrayIn">The image array to convert</param>
        /// <returns>A <seealso cref="System.Drawing.Image"/> that was converted from the image array.</returns>
        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        /// <summary>
        /// Converts an image url into a bute array.
        /// </summary>
        /// <param name="url">The url of the image to be coverted into a <seealso cref="byte[]"/>.</param>
        /// <returns>A byte array converted from the image url.</returns>
        public static byte[] urlToByteArray(string url)
        {
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(url);
            MemoryStream ms = new MemoryStream(bytes);
            return imageToByteArray(System.Drawing.Image.FromStream(ms));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static Image urlToImage(string url) => byteArrayToImage(urlToByteArray(url));
    }
}

