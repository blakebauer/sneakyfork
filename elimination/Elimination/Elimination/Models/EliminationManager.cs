﻿using Elimination.Controllers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Elimination.Models
{
    public class EliminationManager
    {
        EliminationDBContext db;

        //Model specific variables
        private bool validElimination = false;
        private int targetCode = -1;
        private int playerID;
        private Player player;
        private Player eliminatedPlayer;
        private Game game;
        private string photoUrl;

        public EliminationManager(JsonElimination jsonElimination, EliminationDBContext db)
        {
            this.photoUrl = jsonElimination.PhotoURL;
            this.db = db;
            this.playerID = jsonElimination.PlayerID;
            this.game = db.Games.Where(g => g.GameID == jsonElimination.GameID).FirstOrDefault();
            this.targetCode = jsonElimination.TargetCode;
        }

        /// <summary>
        /// This method checks to see if the the targetCode provided is valid before moving on to anything else.
        /// If the targetCode is valid then it will proceed to check if the photo provided is valid as well. If the
        /// photo is valid, then the mothod will eliminate the player and return the new target. If the photo wasn't 
        /// valid, then then method will return a <seealso cref="EliminationConfirmation.Confirmation"/> with an "Error" value.
        /// </summary>
        /// <returns>A <seealso cref="EliminationConfirmation"/> with then next target if successful elimination occurs. Otherwise
        /// it will return a <seealso cref="EliminationConfirmation.Confirmation"/> with an "Error" value.
        /// </returns>
        public async Task<EliminationConfirmation> EliminatePlayer()
        {
            //Check if the targetCode is valid
            if(isValidTargetCode())
            {
                //Convert the url provided into a byte array
                byte[] img = ImageConverter.urlToByteArray(this.photoUrl);

                //Check to see the confidence of the photo validity
                var sf = await FacialRecognition.Compare(img, this.eliminatedPlayer.PhotoUrl);
                if (sf.Confidence > 0.75)
                {
                    //Photo is good to go so eliminate the player
                    game.Eliminations.Add(this.game.EliminatePlayer(this.eliminatedPlayer.PlayerID));
                    db.Entry(game).State = EntityState.Modified;
                    db.SaveChanges();

                    var newTarget = this.game.Targets.Where(t => t.APlayer.PlayerID == this.playerID).FirstOrDefault();

                    //return new target information
                    return new EliminationConfirmation
                    {
                        Confirmation = "OK",
                        NextTargetID = newTarget.TargetID,
                        TargetName = newTarget.TargetPlayer.FirstName,
                        TargetLastName = newTarget.TargetPlayer.LastName,
                        TargetPhoto = newTarget.TargetPlayer.PhotoUrl
                    };
                }
            }

            return new EliminationConfirmation
            {
                Confirmation = "Error",
                NextTargetID = -1,
                TargetName = "",
                TargetLastName = "",
                TargetPhoto = ""
            };
        }

        private bool isValidTargetCode()
        {
            try
            {
                //Here we check to see that both players are in the game and that the targetCode is valid
                this.player = this.game.PlayersLists.Where(pl => pl.PlayerID == this.playerID).FirstOrDefault().Player;
                this.eliminatedPlayer = this.game.PlayersLists.Where(pl => pl.TargetCode == this.targetCode).FirstOrDefault().Player;

                this.validElimination = this.player != null && this.eliminatedPlayer != null;
            } 
            catch (NullReferenceException e)
            {
                return this.validElimination = false;
            }

            return this.validElimination;
        }

        private bool eliminatePlayer()
        {
            if(validElimination)
            {
                //Gets the PlayerID of the player to be eliminated by going into the 
                //Players PlayersList where the GameID == this.gameID and the PlayersList targetCode == this.targetCode
                int eliminatedID = db.Players.Where(
                    p => p.PlayersLists.All(
                        pl => pl.GameID == this.game.GameID && 
                        pl.TargetCode == this.targetCode))
                        .FirstOrDefault().PlayerID;

                this.game.EliminatePlayer(eliminatedID);
                db.Entry(game);
                db.SaveChanges();
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}