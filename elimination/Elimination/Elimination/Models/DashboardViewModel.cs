﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public class DashboardViewModel
    {
        public Player player { get; set; }
        public string photoUrl;
        public List<Game> games { get; set; }
        public IEnumerable<Game> hostedGames { get; set; }
        public Game mostRecentGame { get; set; }
        public int gamesHosted;
        public int gamesPlayed;
        public int targetsEliminated;
        public int timesEliminated;
        public int targetCode { get; set; }
        public int gameID
        {
            get
            {
                return mostRecentGame.GameID;
            }
            set
            {
                mostRecentGame.GameID = value;
            }
        }

        public DashboardViewModel(Player player, string photoUrl, List<Game> games, 
            Game mostRecentGame, int gamesHosted, int gamesPlayed, int targetsEliminated, int timesEliminated, int targetCode)
        {
            this.player = player;
            this.games = games;
            this.mostRecentGame = mostRecentGame;
            this.photoUrl = photoUrl; //for displaying image on dash.
            this.gamesHosted = gamesHosted;
            this.gamesPlayed = gamesPlayed;
            this.targetsEliminated = targetsEliminated;
            this.timesEliminated = timesEliminated;
            this.hostedGames = player.Games.Where(g => g.Host == player.PlayerID).ToList();
            this.targetCode = targetCode;
        }
    }
}