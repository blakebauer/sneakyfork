﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public class ImageEliminVM
    {
        public string ImageUrl { get; set; }
        public int PlayerID { get; set; }
        public int GameID { get; set; }

        public ImageEliminVM(string img, int pID, int gID)
        {
            ImageUrl = img;
            PlayerID = pID;
            GameID = gID;
        }
    }
}