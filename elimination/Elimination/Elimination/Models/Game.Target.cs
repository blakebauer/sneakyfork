﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public partial class Game
    {
        /// <summary>
        /// Adds a player to the game and assigns its target.
        /// Will not save the DB since it cannot have access
        /// to the context class
        /// </summary>
        /// <param name="p">The player to be added</param>
        public void AddPlayer(Player p)
        {
            if(Targets.Count == 0 && Players.Count == 1)
            {
                Target t1 = new Target
                {
                    Player = p.PlayerID,
                    TargetID = Players.FirstOrDefault().PlayerID,
                    GameID = this.GameID
                };
                Target t2 = new Target
                {
                    Player = Players.FirstOrDefault().PlayerID,
                    TargetID = p.PlayerID,
                    GameID = this.GameID
                };
                Targets.Add(t1);
                Targets.Add(t2);
            }
            else if (Targets.Count > 0)
            {
                Target t = Targets.FirstOrDefault();
                int targetID = t.TargetID;

                Target t1 = new Target
                {
                    Player = p.PlayerID,
                    TargetID = targetID,
                    GameID = this.GameID
                };
                Target t2 = new Target
                {
                    Player = t.Player,
                    TargetID = p.PlayerID,
                    GameID = this.GameID
                };
                Targets.Remove(t);
                Targets.Add(t1);
                Targets.Add(t2);
            }
            Players.Add(p);
        }

        /// <summary>
        /// Removes the player from the game and removes 
        /// reassigns the targets
        /// </summary>
        /// <param name="playerID"></param>
        public void RemovePlayer(int playerID)
        {
            var predator = Targets.FirstOrDefault(t => t.TargetID == playerID);
            var prey = Targets.FirstOrDefault(t => t.Player == playerID);

            if(predator != null)
            {
                if (predator.Player != prey.TargetID)
                {
                    Target t = new Target
                    {
                        Player = predator.Player,
                        TargetID = prey.TargetID,
                        GameID = this.GameID
                    };
                    Targets.Add(t);
                }

                Targets.Remove(predator);
                Targets.Remove(prey);
            }
            
            Players.Remove(Players.FirstOrDefault(p => p.PlayerID == playerID));
            PlayersLists.Remove(PlayersLists.FirstOrDefault(p => p.PlayerID == playerID));
        }

        /// <summary>
        /// Elminates a Player will send a win condition if met
        /// Updates stats of both Players involved.
        /// </summary>
        /// <param name="playerID">The Victim</param>
        /// <returns>A new elmination object that will need to be added to the db</returns>
        public Elimination EliminatePlayer(int playerID)
        {
            Target elminatedTarget = Targets.FirstOrDefault(t => t.TargetID == playerID);
            Target nextTarget = Targets.FirstOrDefault(t => t.Player == playerID);
            Player elminator = Players.FirstOrDefault(p => p.PlayerID == elminatedTarget.Player);
            Player elminated = Players.FirstOrDefault(p => p.PlayerID == playerID);

            Elimination elminination = new Elimination()
            {
                Eliminated = playerID,
                Eliminator = elminator.PlayerID,
                GameID = GameID
            };

            if (nextTarget.TargetID == elminatedTarget.Player)
            {
                // TODO: add game win functionaility 
            }
            else
            {
                Target newTarget = new Target
                {
                    Player = elminator.PlayerID,
                    TargetID = nextTarget.TargetID,
                    GameID = GameID
                };
                Targets.Add(newTarget);
                
                // TODO: Add stats
            }

            Targets.Remove(nextTarget);
            Targets.Remove(elminatedTarget);

            return elminination;
        }
    }
}