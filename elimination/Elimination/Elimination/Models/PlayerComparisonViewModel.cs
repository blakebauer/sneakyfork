﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public class PlayerComparisonViewModel
    {
        public Player player { get; set; }
        public string photoBase64;
        public List<Game> games { get; set; }
        public Game mostRecentGame { get; set; }
        public int gamesHosted;
        public int gamesPlayed;
        public int targetsEliminated;
        public int timesEliminated;

        /*another player for comparison*/
        public Player player2 { get; set; }
        public string photoBase642;
        public List<Game> games2 { get; set; }
        public Game mostRecentGame2 { get; set; }
        public int gamesHosted2;
        public int gamesPlayed2;
        public int targetsEliminated2;
        public int timesEliminated2;

        public PlayerComparisonViewModel(Player player, string photoBase64, List<Game> games,
            Game mostRecentGame, int gamesHosted, int gamesPlayed, int targetsEliminated, int timesEliminated,
            Player player2, string photoBase642, List<Game> games2,
            Game mostRecentGame2, int gamesHosted2, int gamesPlayed2, int targetsEliminated2, int timesEliminated2)
        {
            this.player = player;

            this.games = games;

            this.mostRecentGame = mostRecentGame;

            this.photoBase64 = photoBase64; //for displaying image on dash.

            this.gamesHosted = gamesHosted;

            this.gamesPlayed = gamesPlayed;

            this.targetsEliminated = targetsEliminated;

            this.timesEliminated = timesEliminated;

            //the other player
            this.player2 = player2;

            this.games2 = games2;

            this.mostRecentGame2 = mostRecentGame2;

            this.photoBase642 = photoBase642;

            this.gamesHosted2 = gamesHosted2;

            this.gamesPlayed2 = gamesPlayed2;

            this.targetsEliminated2 = targetsEliminated2;

            this.timesEliminated = timesEliminated;
        }
    }
}