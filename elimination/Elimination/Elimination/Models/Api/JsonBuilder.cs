﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Web;

namespace Elimination.Models.Api
{
    public class JsonBuilder
    {
        public static JsonGame BuildGame(Game game)
        {
            return new JsonGame
            {
                GameID = game.GameID,
                Name = game.Name,
                Host = game.Host,
                Active = game.Active,
                AccesLevel = game.AccesLevel,
                StartDate = game.StartDate,
                EndDate = game.EndDate,
                Location = game.Location,
                LocationCenterLong = game.LocationCenterLong,
                LocatoinCenterLat = game.LocatoinCenterLat,
                LocationRadius = game.LocationRadius,
                Players = BuildPlayers(game)
            };
        }

        //public static Game BuildGameFromJson(string jsonGame)
        //{

        //    Game game = null;
        //    try
        //    {
        //        JsonGame jGame = JsonConvert.DeserializeObject<JsonGame>(jsonGame);
        //        game = new Game
        //        {
        //            GameID = game.GameID,
        //            Name = game.Name,
        //            Host = game.Host,
        //            Active = game.Active,
        //            AccesLevel = game.AccesLevel,
        //            StartDate = game.StartDate,
        //            EndDate = game.EndDate,
        //            Location = game.Location,
        //            LocationCenterLong = game.LocationCenterLong,
        //            LocatoinCenterLat = game.LocatoinCenterLat,
        //            LocationRadius = game.LocationRadius,
        //            Players = jGame.Players.ToList()
        //        };
        //    }
        //    catch (Exception e) { }
        //}

        public static List<JsonGame> BuildGames(List<Game> games)
        {
            List<JsonGame> gameList = new List<JsonGame>();

            foreach (var game in games)
                gameList.Add(BuildGame(game));

            return gameList;
        }

        public static JsonPlayer BuildPlayer(Player player)
        {
            return new JsonPlayer
            {
                PlayerID = player.PlayerID,
                FirstName = player.FirstName,
                LastName = player.LastName,
                UserName = player.UserName,
                Email = player.Email,
                Phone = player.Phone,
                DOB = player.DOB,
                PhotoUrl = player.PhotoUrl,
                Profile = player.Profile,
                AuthUserID = player.AuthUserID
            };
        }

        public static List<JsonPlayer> BuildPlayers(Game game)
        {
            List<JsonPlayer> jPlayers = new List<JsonPlayer>();

            foreach (var playersList in game.PlayersLists)
                jPlayers.Add(BuildPlayer(playersList.Player));

            return jPlayers;
        }

        //public static List<Player> BuildPlayersFromJsonPlayer(List<JsonPlayer> jPlayers)
        //{

        //}
    }
}