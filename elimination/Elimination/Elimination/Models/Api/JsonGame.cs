﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Elimination.Models.Api
{
    [DataContract]
    public class JsonGame
    {
        [DataMember]
        public int GameID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Host { get; set; }

        [DataMember]
        public bool Active { get; set; }

        [DataMember]
        public bool? AccesLevel { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public decimal LocationCenterLong { get; set; }

        [DataMember]
        public decimal LocatoinCenterLat { get; set; }

        [DataMember]
        public int LocationRadius { get; set; }

        [DataMember]
        public List<JsonPlayer> Players { get; set; }
    }
}