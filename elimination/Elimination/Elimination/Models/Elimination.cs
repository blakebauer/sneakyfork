namespace Elimination.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Elimination
    {
        public int EliminationID { get; set; }

        public int Eliminator { get; set; }

        public int Eliminated { get; set; }

        public decimal LocationCenterLong { get; set; }

        public decimal LocatoinCenterLat { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] TimeStamp { get; set; }

        public int GameID { get; set; }

        public virtual Player EEliminated { get; set; }

        public virtual Player EEliminator { get; set; }

        public virtual Game Game { get; set; }
    }

    public class JsonElimination
    {
        public int PlayerID { get; set; }
        public int GameID { get; set; }
        public int TargetCode { get; set; }
        public string PhotoURL { get; set; }
    }

    public class EliminationConfirmation
    {
        public string Confirmation { get; set; }
        public int NextTargetID { get; set; }
        public string TargetName { get; set; }
        public string TargetLastName { get; set; }
        public string TargetPhoto { get; set; }
    }
}
