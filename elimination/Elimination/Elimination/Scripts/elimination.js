/*******************Register page drag-n-drop functionality**********************/
$(document).ready(function () {

    /*datepicker*/
    $(function () {
        $("#datepicker").datepicker({
            changeMont: true
            , changeYear: true
            , yearRange: "-110:+0"
        });
    });

    $('#add-players-button').bind('click', function () {
        var nameIds = $('.selectpicker').val();
        var gameID = $('#add-players-button').val();
        var currpid = $('#player-id-holder').attr('data-player-id');

        console.log("Making ajax call to games controller asking for players with ids: " + nameIds);

        if (nameIds === null) {
            console.log("Names are null");
        }

        $.ajax({
            type: "POST"
            , datatype: "json"
            , url: "AddPlayers"
            , data: { ids: nameIds, gid: gameID, currPID: currpid }
            , success: function (players) {
                console.log("First player is: " + players[0].UserName);
                populateGamePlayers(players);
            }
            , error: function () {

            }
        });

        console.log("The names are: " + nameIds);
    });

    $('#populate-image-button').bind('click', function () {
        console.log("Registering data");

        $.ajax({
            type: "POST"
            , datatype: "json"
            , url: "CreateABunchOfData"
            , data: {id: 1}
            , success: function (value) {
                console.log("Data Creation is: " + value);
            }
            , error: function () {

            }
        });
    });
});

function openFileSelector() {
    console.log("clicked photo");

    $("input:file").click();
    var file = $('input:file').prop('files');
    console.log("some stuff");
}

function setSelectedPlayerNames() {
    console.log("Setting names");
    $('.selectpicker').selectpicker('val', 'Jim Lame');
    $('.selectpicker').selectpicker('refresh');
}

function sendInvites(sendType) {
    var gameID = "";
    if (sendType === 'email')
        gameID = $('#email-invites-button').val();
    else
        gameID = $('#sms-invites-button').val();

    var nameIds = $('.selectpicker').val();
    var currpid = $('#player-id-holder').attr('data-player-id');

    console.log("Making ajax call to send emails/sms: " + nameIds);

    $.ajax({
        type: "POST"
        , datatype: "json"
        , url: "../InvitePlayers"
        , data: { gameID: gameID, sendType: sendType }
        , success: function (status) {
            console.log("Email status is: " + status);
        }
        , error: function () {

        }
    });
}


function populateGamePlayers(players) {
    $('#game-players-table').find("tr:gt(0)").remove();
    var i = 0;
    for (i = 0; i < players.length; i++) {
        var row = $('<tr>');

        var userName = $('<td>' + players[i].UserName + '</td>');
        var email = $('<td>' + players[i].Email + '</td>');
        var phone = $('<td>' + players[i].Phone + '</td>');

        row.append(userName);
        row.append(email);
        row.append(phone);
        row.append('</r>');

        $('#game-players-table').append(row);
    }
}


/*==========================================================================*/
/*************************Drag drop stuff************************************/
/*==========================================================================*/

$(document).ready(function () {

    $('#delete-image-button').hide();

    // Makes sure the dataTransfer information is sent when we
    // Drop the item in the drop box.
    jQuery.event.props.push('dataTransfer');

    // Bind the drop event to the dropzone.
    $('#drop-files').bind('drop', function (e) {
        displayPhoto(e);
    });

    function restartFiles() {

        // This is to set the loading bar back to its default state
        $('#loading-bar .loading-color').css({ 'width': '0%' });
        $('#loading').css({ 'display': 'none' });
        $('#loading-content').html(' ');
        $('#drop-files').show();
        $('#delete-image-button').hide();
        $('#Photo').attr('value', '');
        $('#PhotoExtension').attr('value', '');
        // --------------------------------------------------------

        // We need to remove all the images and li elements as
        // appropriate. We'll also make the upload button disappear

        $('#upload-button').hide();
        $('#dropped-files > .image').remove();
        $('#extra-files #file-list li').remove();
        $('#extra-files').hide();
        $('#uploaded-holder').hide();

        return false;
    }

    // Just some styling for the drop file container.
    $('#drop-files').bind('dragenter', function () {
        $(this).css({ 'box-shadow': 'inset 0px 0px 20px rgba(0, 0, 0, 0.1)', 'border': '4px dashed #bb2b2b' });
        return false;
    });

    $('#drop-files').bind('drop', function () {
        $(this).css({ 'box-shadow': 'none', 'border': '4px dashed rgba(0,0,0,0.2)' });
        return false;
    });

    // For the file list
    $('#extra-files .number').toggle(function () {
        $('#file-list').show();
    }, function () {
        $('#file-list').hide();
    });

    $('#delete-image-button').bind('click', function () {
        restartFiles();
        $('#register-button')[0].disabled = true;
        $('#facestatus')[0].innerHTML = "";
    });
});

function displayPhoto(e) {
    // Makes sure the dataTransfer information is sent when we
    // Drop the item in the drop box.
    jQuery.event.props.push('dataTransfer');

    // The number of images to display
    var maxFiles = 1;
    var errMessage = 0;
    var imageWidth = 400;

    // Stop the default action, which is to redirect the page
    // To the dropped file
    var files = e.dataTransfer.files;

    $('#delete-image-button').show();

    $('#drop-files').hide();

    // Show the upload holder
    $('#uploaded-holder').show();



    // For each file
    $.each(files, function (index, file) {

        // Some error messaging
        if (!files[index].type.match('image.*')) {
            $('#drop-files').html('Hey! Images only');
            return false;
        }

        // Start a new instance of FileReader
        var fileReader = new FileReader();

        // When the filereader loads initiate a function
        fileReader.onload = (function (file) {

            return function (photo) {
                // Move each image 40 more pixels across
                var image = this.result;

                // Place extra files in a list
                if ($('#dropped-files > .image').length < maxFiles) {
                    //Set left margin for photo to appear in center
                    var left = $('#drop-files').width();
                    console.log("width is: " + left);
                    left = (left / 2) - (imageWidth / 2);

                    // Place the image inside the dropzone
                    $('#dropped-files').append('<div class="image form-control" type="image" id="ImageFile" name="ImageFile" ' +
                        'style = "top: 0px; margin-left:' + left + 'px; background: url(' + image + '); background-size: cover;" > </div > ');

                    //Checks to see what type of image format it is and removes the proper header
                    //Adds the image extension to an invisible elements for the controller to use
                    if (image.includes('data:image/jpeg;base64,')) {
                        $('#Photo').attr('value', image.replace('data:image/jpeg;base64,', ''));
                        $('#PhotoExtension').attr('value', '.jpeg');
                    } else if (image.includes('data:image/png;base64,')) {
                        $('#Photo').attr('value', image.replace('data:image/png;base64,', ''));
                        $('#PhotoExtension').attr('value', '.png');
                    } else if (image.includes('data:image/jpg;base64,')) {
                        $('#Photo').attr('value', image.replace('data:image/jpg;base64,', ''));
                        $('#PhotoExtension').attr('value', '.jpg');
                    }

                    $('#facestatus')[0].innerHTML = "Checking face status...";
                    var img123 = $("#Photo")[0].value;

                    var a = $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "../Games/IsFaceAJAX",
                        data: JSON.stringify({ img: img123 }),
                        contentType: 'application/json; charset=utf-8',
                        async: true,
                        processData: false
                    }).done(function () {
                        var face = JSON.parse(a.responseJSON);
                        if (face.isFace) {
                            $('#facestatus')[0].innerHTML = "Image is a face!";
                            $('#register-button')[0].disabled = false;
                        } else {
                            $('#facestatus')[0].innerHTML = "Image is not a face. Please upload your face!";
                            $('#register-button')[0].disabled = true;
                        }
                    });
                }
            };

        })(files[index]);

        // For data URI purposes
        fileReader.readAsDataURL(file);
    });
}