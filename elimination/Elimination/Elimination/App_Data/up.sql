-- Exported from QuickDBD: https://www.quickdatatabasediagrams.com/
-- Link to schema: https://app.quickdatabasediagrams.com/#/schema/RVPOZ4sM_k-Bhk237T6VpA
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.

IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'Elimination')
	CREATE DATABASE [Elimination]
GO

USE [Elimination];
GO

/*********************Start of down script************************/

IF OBJECT_ID('dbo.Players', 'U') IS NOT NULL
BEGIN TRY
	IF(OBJECT_ID('FK_Eliminations_Eliminator', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Eliminations] DROP CONSTRAINT [FK_Eliminations_Eliminator]
	END
	IF(OBJECT_ID('FK_Eliminations_Eliminated', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Eliminations] DROP CONSTRAINT [FK_Eliminations_Eliminated]
	END
	IF(OBJECT_ID('FK_Attempts_Eliminator', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Attempts] DROP CONSTRAINT [FK_Attempts_Eliminator]
	END
	IF(OBJECT_ID('FK_Attempts_Eliminated', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Attempts] DROP CONSTRAINT [FK_Attempts_Eliminated]
	END
	IF(OBJECT_ID('FK_Targets_Player', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Targets] DROP CONSTRAINT [FK_Targets_Player]
	END
	IF(OBJECT_ID('FK_Targets_Target', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Targets] DROP CONSTRAINT [FK_Targets_Target]
	END
	IF(OBJECT_ID('FK_PlayersLists_PlayerID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayersLists] DROP CONSTRAINT [FK_PlayersLists_PlayerID]
	END
	IF(OBJECT_ID('FK_PlayersFriends_Player', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayersFriends] DROP CONSTRAINT [FK_PlayersFriends_Player]
	END
	IF(OBJECT_ID('FK_PlayersFriends_Friend', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayersFriends] DROP CONSTRAINT [FK_PlayersFriends_Friend]
	END
	IF(OBJECT_ID('FK_RequestingPlayers_PlayerID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [RequestingPlayers] DROP CONSTRAINT [FK_RequestingPlayers_PlayerID]
	END
	IF(OBJECT_ID('FK_Host_HostID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Games] DROP CONSTRAINT [FK_Host_HostID]
	END
	DROP TABLE IF EXISTS [Players];
END TRY
BEGIN CATCH
	PRINT 'Could not delete Players table';
END CATCH;
GO

IF OBJECT_ID('dbo.Games', 'U') IS NOT NULL
BEGIN TRY 
	IF(OBJECT_ID('FK_PlayersLists_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [PlayersLists] DROP CONSTRAINT [FK_PlayersLists_GameID]
	END
	IF(OBJECT_ID('FK_RequestingPlayers_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [RequestingPlayers] DROP CONSTRAINT [FK_RequestingPlayers_GameID]
	END
	IF(OBJECT_ID('FK_Eliminations_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Eliminations] DROP CONSTRAINT [FK_Eliminations_GameID]
	END
	IF(OBJECT_ID('FK_Targets_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Targets] DROP CONSTRAINT [FK_Targets_GameID]
	END
	IF(OBJECT_ID('FK_Attempts_GameID', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE [Attempts] DROP CONSTRAINT [FK_Attempts_GameID]
	END
	DROP TABLE IF EXISTS [Games]
END TRY
BEGIN CATCH
	PRINT 'Could not delete Game table';
END CATCH;
GO

IF OBJECT_ID('dbo.PlayersLists', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [PlayersLists];
END TRY
BEGIN CATCH
	PRINT 'Could not delete PlayersLists table';
END CATCH;
GO

IF OBJECT_ID('dbo.Eliminations', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [Eliminations];
END TRY
BEGIN CATCH
	PRINT 'Could not delete Eliminations table';
END CATCH;
GO

IF OBJECT_ID('dbo.Targets', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [Targets];
END TRY
BEGIN CATCH
	PRINT 'Could not delete Targets table';
END CATCH;
GO

IF OBJECT_ID('dbo.Attempts', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [Attempts];
END TRY
BEGIN CATCH
	PRINT 'Could not delete Attempts table';
END CATCH;
GO

IF OBJECT_ID('dbo.PlayersFriends', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [PlayersFriends];
END TRY
BEGIN CATCH
	PRINT 'Could not delete PlayersFriends table';
END CATCH;
GO

IF OBJECT_ID('dbo.RequestingPlayers', 'U') IS NOT NULL
BEGIN TRY 
	DROP TABLE IF EXISTS [RequestingPlayers];
END TRY
BEGIN CATCH
	PRINT 'Could not delete RequestingPlayers table';
END CATCH;
GO

/*********************End of down script************************/


/*********************Start of up script************************/
SET XACT_ABORT ON

BEGIN TRANSACTION QUICKDBD

CREATE TABLE [Players] (
    [PlayerID]		INT IDENTITY(1,1)	NOT NULL
    ,[FirstName]	NVARCHAR(64)		NOT NULL
    ,[LastName]		NVARCHAR(64)		NOT NULL
    ,[UserName]		NVARCHAR(64)		NOT NULL
    ,[Email]		NVARCHAR(96)		NOT NULL
    ,[Phone]		NVARCHAR(20)		NOT NULL
    ,[DOB]			DATETIME			NOT	NULL
    ,[PhotoUrl]		VARCHAR(max)		NULL
    ,[Profile]		NVARCHAR(256)		NOT NULL
	,[AuthUserID]	NVARCHAR(128)		NOT NULL
    ,CONSTRAINT [PK_Players] PRIMARY KEY CLUSTERED ([PlayerID] ASC)
)

CREATE TABLE [PlayersFriends] (
    [Player]			INT  NOT NULL
    ,[PlayerFiend]		INT  NOT NULL
	,CONSTRAINT [PK_PlayersFriends] PRIMARY KEY CLUSTERED ([Player], [PlayerFiend])
)

CREATE TABLE [PlayersLists] (
    [GameID]		INT  NOT NULL
    ,[PlayerID]		INT  NOT NULL
	,[TargetCode]	INT	 NULL
	,CONSTRAINT [PK_PlayersLists] PRIMARY KEY CLUSTERED ([GameID], [PlayerID])
)

CREATE TABLE [RequestingPlayers] (
    [GameID]				INT  NOT NULL
    ,[RequestingPlayer]		INT  NOT NULL
	,CONSTRAINT [PK_RequestingPlayers] PRIMARY KEY CLUSTERED ([GameID], [RequestingPlayer])
)

CREATE TABLE [Eliminations] (
    [EliminationID]			INT IDENTITY(1,1)	NOT NULL
    ,[Eliminator]			INT							NOT NULL
    ,[Eliminated]			INT							NOT NULL
    ,[LocationCenterLong]	DECIMAL(9,6)				NOT NULL
    ,[LocatoinCenterLat]	DECIMAL(9,6)				NOT NULL
    ,[TimeStamp]			TIMESTAMP					NOT NULL
    ,[GameID]				INT							NOT NULL
    ,CONSTRAINT [PK_Eliminations] PRIMARY KEY CLUSTERED ([EliminationID] ASC)
)

CREATE TABLE [Games] (
    [GameID]				INT IDENTITY(1,1)	NOT NULL
    ,[Name]					NVARCHAR(64)				NOT NULL
	,[Host]					INT							NOT NULL
	,[Active]				BIT							NOT NULL
	,[AccesLevel]			BIT							NULL
    ,[StartDate]			DATETIME					NOT NULL
    ,[EndDate]				DATETIME					NULL
    ,[Location]				NVARCHAR(64)				NOT NULL
    ,[LocationCenterLong]	decimal(9,6)				NOT NULL
    ,[LocatoinCenterLat]	DECIMAL(9,6)				NOT NULL
    ,[LocationRadius]		INT							NOT NULL
    ,CONSTRAINT [PK_Games] PRIMARY KEY CLUSTERED ([GameID] ASC)
)

CREATE TABLE [Targets] (
    [GameID] INT  NOT NULL
    ,[Player] INT  NOT NULL
    ,[Target] INT  NOT NULL
	,CONSTRAINT [PK_Targest] PRIMARY KEY CLUSTERED ([GameID], [Player], [Target])
)

CREATE TABLE [Attempts] (
    [AttemptID]				INT IDENTITY(1,1)	NOT NULL
    ,[Eliminator]			INT							NOT NULL
    ,[Eliminated]			INT							NOT NULL
    ,[LocationCenterLong]	DECIMAL(9,6)				NOT NULL
    ,[LocatoinCenterLat]	DECIMAL(9,6)				NOT NULL
    ,[TimeStamp]			TIMESTAMP					NOT NULL
    ,[GameID]				INT							NOT NULL
    ,CONSTRAINT [PK_Attempts] PRIMARY KEY CLUSTERED ([AttemptID] ASC)
)

ALTER TABLE [PlayersFriends] WITH CHECK ADD CONSTRAINT [FK_PlayersFriends_Player] FOREIGN KEY([Player])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [PlayersFriends] CHECK CONSTRAINT [FK_PlayersFriends_Player]


ALTER TABLE [PlayersFriends] WITH CHECK ADD CONSTRAINT [FK_PlayersFriends_Friend] FOREIGN KEY([PlayerFiend])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [PlayersFriends] CHECK CONSTRAINT [FK_PlayersFriends_Friend]


ALTER TABLE [PlayersLists] WITH CHECK ADD CONSTRAINT [FK_PlayersLists_GameID] FOREIGN KEY([GameID])
REFERENCES [Games] ([GameID])
ALTER TABLE [PlayersLists] CHECK CONSTRAINT [FK_PlayersLists_GameID]


ALTER TABLE [PlayersLists] WITH CHECK ADD CONSTRAINT [FK_PlayersLists_PlayerID] FOREIGN KEY([PlayerID])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [PlayersLists] CHECK CONSTRAINT [FK_PlayersLists_PlayerID]


ALTER TABLE [RequestingPlayers] WITH CHECK ADD CONSTRAINT [FK_RequestingPlayers_GameID] FOREIGN KEY([GameID])
REFERENCES [Games] ([GameID])
ALTER TABLE [RequestingPlayers] CHECK CONSTRAINT [FK_RequestingPlayers_GameID]


ALTER TABLE [RequestingPlayers] WITH CHECK ADD CONSTRAINT [FK_RequestingPlayers_PlayerID] FOREIGN KEY([RequestingPlayer])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [RequestingPlayers] CHECK CONSTRAINT [FK_RequestingPlayers_PlayerID]


ALTER TABLE [Eliminations] WITH CHECK ADD CONSTRAINT [FK_Eliminations_Eliminator] FOREIGN KEY([Eliminator])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [Eliminations] CHECK CONSTRAINT [FK_Eliminations_Eliminator]


ALTER TABLE [Eliminations] WITH CHECK ADD CONSTRAINT [FK_Eliminations_Eliminated] FOREIGN KEY([Eliminated])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [Eliminations] CHECK CONSTRAINT [FK_Eliminations_Eliminated]


ALTER TABLE [Eliminations] WITH CHECK ADD CONSTRAINT [FK_Eliminations_GameID] FOREIGN KEY([GameID])
REFERENCES [Games] ([GameID])
ALTER TABLE [Eliminations] CHECK CONSTRAINT [FK_Eliminations_GameID]


ALTER TABLE [Targets] WITH CHECK ADD CONSTRAINT [FK_Targets_GameID] FOREIGN KEY([GameID])
REFERENCES [Games] ([GameID])
ALTER TABLE [Targets] CHECK CONSTRAINT [FK_Targets_GameID]


ALTER TABLE [Targets] WITH CHECK ADD CONSTRAINT [FK_Targets_Player] FOREIGN KEY([Player])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [Targets] CHECK CONSTRAINT [FK_Targets_Player]


ALTER TABLE [Targets] WITH CHECK ADD CONSTRAINT [FK_Targets_Target] FOREIGN KEY([Target])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [Targets] CHECK CONSTRAINT [FK_Targets_Target]


ALTER TABLE [Attempts] WITH CHECK ADD CONSTRAINT [FK_Attempts_Eliminator] FOREIGN KEY([Eliminator])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [Attempts] CHECK CONSTRAINT [FK_Attempts_Eliminator]


ALTER TABLE [Attempts] WITH CHECK ADD CONSTRAINT [FK_Attempts_Eliminated] FOREIGN KEY([Eliminated])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [Attempts] CHECK CONSTRAINT [FK_Attempts_Eliminated]


ALTER TABLE [Attempts] WITH CHECK ADD CONSTRAINT [FK_Attempts_GameID] FOREIGN KEY([GameID])
REFERENCES [Games] ([GameID])
ALTER TABLE [Attempts] CHECK CONSTRAINT [FK_Attempts_GameID]


ALTER TABLE [Games] WITH CHECK ADD CONSTRAINT [FK_Host_HostID] FOREIGN KEY([Host])
REFERENCES [Players] ([PlayerID])
ALTER TABLE [Games] CHECK CONSTRAINT [FK_Host_HostID]


COMMIT TRANSACTION QUICKDBD