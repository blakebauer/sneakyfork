﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Elimination.Startup))]
namespace Elimination
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
