﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Elimination.Models;
using Microsoft.AspNet.Identity;

namespace Elimination.Controllers
{
    public class PlayersController : Controller
    {
        private EliminationDBContext db = new EliminationDBContext();

        // GET: Players
        public ActionResult Index()
        {
            return View(db.Players.ToList());
        }

        // GET: Players/Details/5
        public ActionResult Details(int? id)
        {
            string IdentityID = User.Identity.GetUserId();
            Player player = db.Players.Where(x => x.AuthUserID.Equals(IdentityID)).FirstOrDefault();

            Player player2 = db.Players.Find(id);//player to be compared to

            if (player2 == null)
            {
                return HttpNotFound();
            } else if (player == null)
            {
                return RedirectToAction("Login", "Account");
            }

            /*process the uploaded photo*/
            string photoBase64, photoBase642;
            if (player.PhotoUrl != null)
            {
                //photoBase64 = Convert.ToBase64String(player.PhotoUrl);
            }
            else
            {
                photoBase64 = "0";
            }
            if (player2.PhotoUrl != null)
            {
                //photoBase642 = Convert.ToBase64String(player2.PhotoUrl);
            }
            else
            {
                photoBase642 = "0";
            }

            /*****get the players played/created games*****/
            List<Game> games = player.Games.ToList();
            Game mostRecentGame = games.FirstOrDefault();//last game played.
            int gamesPlayed = player.PlayersGame.Where(a => a.EndDate >= DateTime.Now).Count();
            int gamesHosted = 0;
            foreach (Game g in games) //find eash game that user is a host, increment games hosted.
            {
                if (g.Host == player.PlayerID)
                {
                    gamesHosted += 1;
                }
            }
            List<Game> games2 = player2.Games.ToList();
            Game mostRecentGame2 = games2.FirstOrDefault();//last game played.
            int gamesPlayed2 = player2.PlayersGame.Where(a => a.EndDate >= DateTime.Now).Count();
            int gamesHosted2 = 0;
            foreach (Game g2 in games2) //find eash game that user is a host, increment games hosted.
            {
                if (g2.Host == player2.PlayerID)
                {
                    gamesHosted += 1;
                }
            }

            /*get the player's eliminations*/
            List<Elimination.Models.Elimination> eliminations = player.EliminationsEliminated.ToList();
            var mostRecentTargetElimination = db.Players.Where(p => p.Equals(eliminations.FirstOrDefault().Eliminated));
            var mostRecentPlayerElimination = db.Eliminations.Where(a => a.Eliminated.Equals(player.PlayerID)).FirstOrDefault();
            int targetsEliminated = player.EliminationsEliminated.Where(t => t.Eliminator.Equals(player.PlayerID)).Count();
            int timesEliminated = player.EliminationsEliminated.Where(v => v.Eliminated.Equals(player.PlayerID)).Count();

            List<Elimination.Models.Elimination> eliminations2 = player2.EliminationsEliminated.ToList();
            var mostRecentTargetElimination2 = db.Players.Where(p => p.Equals(eliminations.FirstOrDefault().Eliminated));
            var mostRecentPlayerElimination2 = db.Eliminations.Where(a => a.Eliminated.Equals(player2.PlayerID)).FirstOrDefault();
            int targetsEliminated2 = player2.EliminationsEliminated.Where(t => t.Eliminator.Equals(player2.PlayerID)).Count();
            int timesEliminated2 = player2.EliminationsEliminated.Where(v => v.Eliminated.Equals(player2.PlayerID)).Count();

            return View(new PlayerComparisonViewModel(player, /*photoBase64*/"", games, mostRecentGame, gamesHosted, gamesPlayed, targetsEliminated, timesEliminated,
                player2, /*photoBase642*/"", games2, mostRecentGame2, gamesHosted2, gamesPlayed2, targetsEliminated2, timesEliminated2));
        }

            // GET: Players/Create
            public ActionResult Create()
        {
            return View();
        }

        // POST: Players/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PlayerID,FirstName,LastName,UserName,Email,Phone,DOB,PhotoUrl,Profile,AuthUserID")] Player player)
        {
            if (ModelState.IsValid)
            {
                db.Players.Add(player);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(player);
        }

        // GET: Players/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        // POST: Players/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PlayerID,FirstName,LastName,UserName,Email,Phone,DOB,PhotoUrl,Profile,AuthUserID")] Player player)
        {
            if (ModelState.IsValid)
            {
                db.Entry(player).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Dashboard");
            }
            return View(player);
        }

        // GET: Players/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        // POST: Players/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Player player = db.Players.Find(id);
            db.Players.Remove(player);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
