﻿using Elimination.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Elimination.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private EliminationDBContext db = new EliminationDBContext();

        // GET: Dashboard
        public ActionResult Index()
        {
            string IdentityID = User.Identity.GetUserId();
            Player player = db.Players.Where(x => x.AuthUserID.Equals(IdentityID)).FirstOrDefault();

            if (player == null)
            {
                return HttpNotFound();
            }

            /*****get the players played/created games*****/
            List<Game> games = player.PlayersGame.ToList();
            Game mostRecentGame = games.LastOrDefault();//last game played.
            int gamesPlayed = player.PlayersGame.Where(a => a.EndDate >= DateTime.Now).Count();
            int gamesHosted = 0;
            foreach (Game g in games) //find eash game that user is a host, increment games hosted.
            {
                if (g.Host == player.PlayerID)
                {
                    gamesHosted += 1;
                }
            }

            /*get the player's eliminations*/
            List<Elimination.Models.Elimination> eliminations = player.EliminationsEliminated.ToList();
            var mostRecentTargetElimination = db.Players.Where(p => p.Equals(eliminations.FirstOrDefault().Eliminated));
            var mostRecentPlayerElimination = db.Eliminations.Where(a => a.Eliminated.Equals(player.PlayerID)).FirstOrDefault();
            int targetsEliminated = player.EliminationsEliminated.Where(t => t.Eliminator.Equals(player.PlayerID)).Count();
            int timesEliminated = player.EliminationsEliminated.Where(v => v.Eliminated.Equals(player.PlayerID)).Count();

            return View(new DashboardViewModel(player, "", games, mostRecentGame, gamesHosted, gamesPlayed, targetsEliminated, timesEliminated, 0));
      
        }
    }
}