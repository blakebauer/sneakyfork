﻿using Elimination.Models;
using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Elimination.Controllers
{
    public class FacialRecognition
    {
        private static string apikey = WebConfigurationManager.AppSettings["FaceApi"];
        private static string apiroot = "https://westus.api.cognitive.microsoft.com/face/v1.0";

        public static async Task<Face> Upload(string url)
        {
            try
            {
                var faceServiceClient = new FaceServiceClient(apikey, apiroot);
                Face[] faces = await faceServiceClient.DetectAsync(url, returnFaceId: true, returnFaceLandmarks: false);
                return faces[0];
            }
            catch (Exception e)
            {
                return new Face();
            }
        }

        public static async Task<Face> Upload(byte[] img)
        {
            Image imgObj = Models.ImageConverter.byteArrayToImage(img);
            var imgArray = Models.ImageConverter.imageToByteArray(imgObj);

            try
            {
                var faceServiceClient = new FaceServiceClient(apikey, apiroot);
                Stream stm = new MemoryStream(imgArray);
                Face[] faces = await faceServiceClient.DetectAsync(stm, returnFaceId: true, returnFaceLandmarks: false);
                if (faces.Length > 0)
                    return faces[0];
                else
                    return new Face();
            }
            catch (Exception e)
            {
                return new Face();
            }
        }

        public static async Task<SimilarFace> Compare(byte[] img, string url)
        {
            Face faceImg = await Upload(img);
            Face faceUrl = await Upload(url);

            try
            {
                if(faceImg.FaceId != null && faceUrl.FaceId != null)
                {
                    var faceServiceClient = new FaceServiceClient(apikey, apiroot);
                    SimilarFace[] sfs = await faceServiceClient.FindSimilarAsync(faceImg.FaceId, new Guid[] { faceUrl.FaceId });
                    return sfs[0];
                } else
                {
                    return new SimilarFace();
                }
            }
            catch (Exception e)
            {
                return new SimilarFace();
            }
        }

        /*public async Task<bool> EliminatePlayer(int gameID, int playerID, byte[] img)
        {
            using (var db = new EliminationDBContext())
            {
                var url = db.Players.Find(new { playerID }).PhotoUrl;

                var sf = await Compare(img, url);
                if (sf.Confidence > 0.75)
                {
                    var game = db.Games.Find(new { gameID });
                    game.ElminatePlayer(playerID);
                    db.Entry(game).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }*/

        public static async Task<string> IsFace(byte[] img)
        {
            Face f = await Upload(img);
            if (f.FaceRectangle == null)
                return JsonConvert.SerializeObject(new { isFace = false });
            return JsonConvert.SerializeObject(new { isFace = true, Rectangle = f.FaceRectangle});
        }
    }
}