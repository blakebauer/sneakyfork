﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Elimination.Models;
using Elimination.Models.Api;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;

namespace Elimination.Controllers.Api
{
    public class GamesController : ApiController
    {
        private EliminationDBContext db = new EliminationDBContext();

        // GET api/Games
        public IQueryable<Game> GetGames()
        {
            return db.Games;
        }


        // GET: api/Games/5
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public async Task<IHttpActionResult> GetGame(int id)
        {
            Game game = await db.Games.FindAsync(id);
            if (game == null)
            {
                return NotFound();
            }

            return Ok(JsonBuilder.BuildGame(game));
        }

        // GET: api/Games/5
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public async Task<IHttpActionResult> GetGameList(string ids)
        {
            string[] idArray = null;

            try
            {
                idArray = ids.Split(',').ToArray();
            }
            catch (Exception e) { return NotFound(); }

            var games = await db.Games.Where(g => idArray.ToList().Contains(g.GameID.ToString())).ToListAsync();

            List<JsonGame> jsonGames = JsonBuilder.BuildGames(games);

            if (games == null)
            {
                return NotFound();
            }

            return Ok(jsonGames);
        }

        // GET: api/Games/5
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public async Task<IHttpActionResult> GetAllGames()
        {
            var games = await db.Games.ToListAsync();

            List<JsonGame> jsonGames = JsonBuilder.BuildGames(games);

            if (games == null)
            {
                return NotFound();
            }

            return Ok(jsonGames);
        }

        // GET: api/Games/5
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(JsonGame))]
        public async Task<IHttpActionResult> GetAllPlayers(int? id)
        {
            int gameID = id ?? -1;
            var game = await db.Games.FindAsync(gameID);

            List<JsonPlayer> jPlayers = JsonBuilder.BuildPlayers(game);

            if (game == null)
            {
                return NotFound();
            }

            return Ok(jPlayers);
        }

        [Authorize]
        [HttpPost]
        public async Task<IHttpActionResult> JoinGame(int id, LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                //get user id by matching email
                var tempPlayer =  db.AspNetUsers.Where(x => x.Email == model.Email).Select(x=>x.Id).ToList();

                var temp = tempPlayer[0];
                //make Player and set it to current user
                Player player = db.Players.Where(x => x.AuthUserID.Equals(temp)).FirstOrDefault();

                Game game = db.Games.FirstOrDefault(s => s.GameID == id);
                PlayersList pList = new PlayersList
                {
                    Game = game,
                    GameID = game.GameID,
                    Player = player,
                    PlayerID = player.PlayerID,
                    TargetCode = new Random().Next(9999)
                };
               
                    if (game != null)
                    {

                    var host = game.Host;
                    var joined = game.Players;
                   //if player already joined
                    if (joined.Any(x => x.PlayerID == player.PlayerID))   
                    {
                        return BadRequest("Already Joined");
                    }

                    // if player is host
                    if (host.Equals(player.PlayerID))
                    {
                        return BadRequest("You're the host!");
                    }
                    //add player to game Players list
                        game.PlayersLists.Add(pList);
                        game.AddPlayer(player);
                    }
                    db.SaveChanges();
                    return Ok("You've joined");
              
            }
            return BadRequest("Join failed");
        }



        // PUT: api/Games/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGame(int id, Game game)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != game.GameID)
            {
                return BadRequest();
            }

            db.Entry(game).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Games
        [Authorize]
        [ResponseType(typeof(Game))]
        public async Task<IHttpActionResult> PostGame(Game game)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Games.Add(game);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = game.GameID }, game);
        }

        // DELETE: api/Games/5
        [Authorize]
        [ResponseType(typeof(Game))]
        public async Task<IHttpActionResult> DeleteGame(int id)
        {
            Game game = await db.Games.FindAsync(id);
            if (game == null)
            {
                return NotFound();
            }

            db.Games.Remove(game);
            await db.SaveChangesAsync();

            return Ok(game);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GameExists(int id)
        {
            return db.Games.Count(e => e.GameID == id) > 0;
        }
    }
}