﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Elimination.Models;

namespace Elimination.Controllers.Api
{
    public class AspNetUserLoginsController : ApiController
    {
        private EliminationDBContext db = new EliminationDBContext();

        // GET: api/AspNetUserLogins
        public IQueryable<AspNetUserLogin> GetAspNetUserLogins()
        {
            return db.AspNetUserLogins;
        }

        // GET: api/AspNetUserLogins/5
        [ResponseType(typeof(AspNetUserLogin))]
        public async Task<IHttpActionResult> GetAspNetUserLogin(string id)
        {
            AspNetUserLogin aspNetUserLogin = await db.AspNetUserLogins.FindAsync(id);
            if (aspNetUserLogin == null)
            {
                return NotFound();
            }

            return Ok(aspNetUserLogin);
        }

        // PUT: api/AspNetUserLogins/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAspNetUserLogin(string id, AspNetUserLogin aspNetUserLogin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aspNetUserLogin.LoginProvider)
            {
                return BadRequest();
            }

            db.Entry(aspNetUserLogin).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AspNetUserLoginExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AspNetUserLogins
        [ResponseType(typeof(AspNetUserLogin))]
        public async Task<IHttpActionResult> PostAspNetUserLogin(AspNetUserLogin aspNetUserLogin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AspNetUserLogins.Add(aspNetUserLogin);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AspNetUserLoginExists(aspNetUserLogin.LoginProvider))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = aspNetUserLogin.LoginProvider }, aspNetUserLogin);
        }

        // DELETE: api/AspNetUserLogins/5
        [ResponseType(typeof(AspNetUserLogin))]
        public async Task<IHttpActionResult> DeleteAspNetUserLogin(string id)
        {
            AspNetUserLogin aspNetUserLogin = await db.AspNetUserLogins.FindAsync(id);
            if (aspNetUserLogin == null)
            {
                return NotFound();
            }

            db.AspNetUserLogins.Remove(aspNetUserLogin);
            await db.SaveChangesAsync();

            return Ok(aspNetUserLogin);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AspNetUserLoginExists(string id)
        {
            return db.AspNetUserLogins.Count(e => e.LoginProvider == id) > 0;
        }
    }
}