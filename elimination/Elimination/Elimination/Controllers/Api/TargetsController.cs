﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Elimination.Models;

namespace Elimination.Controllers.Api
{
    public class TargetsController : ApiController
    {
        private EliminationDBContext db = new EliminationDBContext();

        // GET: api/Targets
        public IQueryable<Target> GetTargets()
        {
            return db.Targets;
        }

        // GET: api/Targets/5
        [ResponseType(typeof(Target))]
        public async Task<IHttpActionResult> GetTarget(int id)
        {
            Target target = await db.Targets.FindAsync(id);
            if (target == null)
            {
                return NotFound();
            }

            return Ok(target);
        }

        // PUT: api/Targets/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTarget(int id, Target target)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != target.GameID)
            {
                return BadRequest();
            }

            db.Entry(target).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TargetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Targets
        [ResponseType(typeof(Target))]
        public async Task<IHttpActionResult> PostTarget(Target target)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Targets.Add(target);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TargetExists(target.GameID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = target.GameID }, target);
        }

        // DELETE: api/Targets/5
        [ResponseType(typeof(Target))]
        public async Task<IHttpActionResult> DeleteTarget(int id)
        {
            Target target = await db.Targets.FindAsync(id);
            if (target == null)
            {
                return NotFound();
            }

            db.Targets.Remove(target);
            await db.SaveChangesAsync();

            return Ok(target);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TargetExists(int id)
        {
            return db.Targets.Count(e => e.GameID == id) > 0;
        }
    }
}