﻿using Elimination.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Drawing;
using System;
using System.Web.Mvc;
using System.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using Twilio.Jwt.AccessToken;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Net;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;
using System.Text;
using System.IO;
using System.Security;

namespace Elimination.Controllers.Api
{
    public class AccountController : ApiController
    {
        public AccountController()
        {
        }
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private EliminationDBContext db = new EliminationDBContext();

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        public ApplicationSignInManager SignInManager
        {
            get => _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        [System.Web.Http.HttpPost]
        // POST: api/Register
        [ResponseType(typeof(Player))]
        public async Task<IHttpActionResult> Post(RegisterViewModel model)
        { 
            Player player;
            string localPath = "";
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ////////////////////////////////////////////////
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
            var result = await UserManager.CreateAsync(user, model.Password);

            //Initialize Basic storage for storing the image
            BasicStorage basicStorage = new BasicStorage();

            //Convert the array that we get from register page into image and then back to array
            //because we need the proper formatting done by the Image class
            Image img = Models.ImageConverter.byteArrayToImage(model.Photo);

            var imgArray = Models.ImageConverter.imageToByteArray(img);

            //We use this as the image name when retrieving it from the blob storage
            string containerName = BasicStorage.GetContainerName(model);
            string imageName = BasicStorage.GetBlobName(model.PhotoExtension, model);

            //Store on server to be used in BasicSorage Model
            localPath = System.Web.HttpContext.Current.Server.MapPath($"~/bin/{imageName}");
            System.IO.File.WriteAllBytes(localPath, imgArray);

            //Store the image in the Blob database
            await basicStorage.StoreBlob(containerName, imageName, localPath);

            //Get the profile photo url
            Uri photoUri = basicStorage.GetBlob(containerName);

            if (result.Succeeded)
            {
                   await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                player = new Player
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    DOB = model.DOB,
                    UserName = model.UserName,
                    Email = model.Email,
                    Phone = model.Phone,
                    PhotoUrl = photoUri.ToString(),
                    Profile = model.Profile,
                    AuthUserID = user.Id
                };
                db.Players.Add(player);
                db.SaveChanges();

                await db.SaveChangesAsync();
                return CreatedAtRoute("DefaultApi", new { id = player.PlayerID }, player);
            }
            return BadRequest("Something went wrong");
        }

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }


        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}