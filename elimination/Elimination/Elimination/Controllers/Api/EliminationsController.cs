﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Elimination.Models;

namespace Elimination.Controllers.Api
{
    public class EliminationsController : ApiController
    {
        private EliminationDBContext db = new EliminationDBContext();

        // GET: api/Eliminations
        public IQueryable<Models.Elimination> GetEliminations()
        {
            return db.Eliminations;
        }

        // GET: api/Eliminations/5
        [ResponseType(typeof(Models.Elimination))]
        public async Task<IHttpActionResult> GetElimination(int id)
        {
            Models.Elimination elimination = await db.Eliminations.FindAsync(id);
            if (elimination == null)
            {
                return NotFound();
            }

            return Ok(elimination);
        }

        // PUT: api/Eliminations/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutElimination(int id, Models.Elimination elimination)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != elimination.EliminationID)
            {
                return BadRequest();
            }

            db.Entry(elimination).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EliminationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Posts an elimination to be varified and eliminated if it passes the photo recognition 
        /// and a valid targetCode submission.
        /// </summary>
        /// <param name="elimination">Is a minimal version of the <seealso cref="Elimination.Models.Elimination"/> class 
        /// that is used to register an elimination.</param>
        /// <returns>The next target if successful, otherwise an error confirmation will be returned.</returns>
        [ResponseType(typeof(EliminationConfirmation))]
        public async Task<IHttpActionResult> PostElimination(JsonElimination elimination)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //This is where all the magic happens
            EliminationManager manager = new EliminationManager(elimination, db);
            var playerEliminated = await manager.EliminatePlayer();

            return Json(playerEliminated);
        }

        // DELETE: api/Eliminations/5
        [ResponseType(typeof(Models.Elimination))]
        public async Task<IHttpActionResult> DeleteElimination(int id)
        {
            Models.Elimination elimination = await db.Eliminations.FindAsync(id);
            if (elimination == null)
            {
                return NotFound();
            }

            db.Eliminations.Remove(elimination);
            await db.SaveChangesAsync();

            return Ok(elimination);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EliminationExists(int id)
        {
            return db.Eliminations.Count(e => e.EliminationID == id) > 0;
        }
    }
}