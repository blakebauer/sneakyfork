﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Elimination.Models;

namespace Elimination.Controllers.Api
{
    public class AspNetUserClaimsController : ApiController
    {
        private EliminationDBContext db = new EliminationDBContext();

        // GET: api/AspNetUserClaims
        public IQueryable<AspNetUserClaim> GetAspNetUserClaims()
        {
            return db.AspNetUserClaims;
        }

        // GET: api/AspNetUserClaims/5
        [ResponseType(typeof(AspNetUserClaim))]
        public async Task<IHttpActionResult> GetAspNetUserClaim(int id)
        {
            AspNetUserClaim aspNetUserClaim = await db.AspNetUserClaims.FindAsync(id);
            if (aspNetUserClaim == null)
            {
                return NotFound();
            }

            return Ok(aspNetUserClaim);
        }

        // PUT: api/AspNetUserClaims/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAspNetUserClaim(int id, AspNetUserClaim aspNetUserClaim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aspNetUserClaim.Id)
            {
                return BadRequest();
            }

            db.Entry(aspNetUserClaim).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AspNetUserClaimExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AspNetUserClaims
        [ResponseType(typeof(AspNetUserClaim))]
        public async Task<IHttpActionResult> PostAspNetUserClaim(AspNetUserClaim aspNetUserClaim)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AspNetUserClaims.Add(aspNetUserClaim);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = aspNetUserClaim.Id }, aspNetUserClaim);
        }

        // DELETE: api/AspNetUserClaims/5
        [ResponseType(typeof(AspNetUserClaim))]
        public async Task<IHttpActionResult> DeleteAspNetUserClaim(int id)
        {
            AspNetUserClaim aspNetUserClaim = await db.AspNetUserClaims.FindAsync(id);
            if (aspNetUserClaim == null)
            {
                return NotFound();
            }

            db.AspNetUserClaims.Remove(aspNetUserClaim);
            await db.SaveChangesAsync();

            return Ok(aspNetUserClaim);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AspNetUserClaimExists(int id)
        {
            return db.AspNetUserClaims.Count(e => e.Id == id) > 0;
        }
    }
}