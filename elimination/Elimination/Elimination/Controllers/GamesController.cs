﻿using Elimination.Models;
using Microsoft.AspNet.Identity;
using ServerNotifications.Web.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using Elimination.Domain.Twilio;
using Elimination.Models;
using Microsoft.AspNet.Identity;
using Microsoft.ProjectOxford.Face.Contract;
using ServerNotifications.Web.Domain;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Elimination.Controllers
{
    public class GamesController : Controller
    {
        private EliminationDBContext db = new EliminationDBContext();

        // GET: Games
        public ActionResult Index(string searchBy, string search, bool? inActive)
        {
            List<Game> games;
            bool tempInActive;

            if (inActive != null)
            {
                tempInActive = Convert.ToBoolean(inActive);
            }
            else
            {
                tempInActive = false;
            }

            if (tempInActive == false)
            {
                games = db.Games.Include(x => x.Player).ToList();
            }
            else //if( tempInActive == true)
            {
                games = db.Games.Where(x => x.Active == false).Include(x => x.Player).ToList();
            }
            if (searchBy == "Name")
            {
                return View(games.Where(x => x.Name.StartsWith(search) || search == null).ToList());
            }

            else if (searchBy == "location")
            {
                return View(games.Where(x => x.Location.StartsWith(search) || search == null).ToList());
            }
            else
            {
                return View(games.ToList());
            }
        }

        // GET: Games/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            int gameID = id ?? -1;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //the game that matched the id
            Game game = db.Games.Find(gameID);

            //current player id
            string currentp = User.Identity.GetUserId();

            // player element of current player id
            Player player = db.Players.Where(x => x.AuthUserID.Equals(currentp)).FirstOrDefault();
            ViewBag.playerID = player.PlayerID;

            if (game != null && player.AuthUserID.Equals(currentp) && game.Host == player.PlayerID)
            {
                ViewBag.Player = "Host";
            }
            else
            {
                //all players in game
                var joined = game.Players;

                // if already joined
                if (joined.Any(x => x.PlayerID == player.PlayerID))
                {
                    ViewBag.Player = "AlreadyJoined";
                }
                else //not yet joined 
                {
                    ViewBag.Player = "Join";
                }
            }
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details([Bind(Include = "GameID, PlayerID")]  int id)
        {

            string tempPlayer = User.Identity.GetUserId();
            //make Player and set it to current user
            Player player = db.Players.Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();

            Game game = db.Games.FirstOrDefault(s => s.GameID == id);
            PlayersList pList = new PlayersList
            {
                Game = game,
                GameID = game.GameID,
                Player = player,
                PlayerID = player.PlayerID,
                TargetCode = new Random().Next(9999)
            };

            if (ModelState.IsValid)
            {
                if (game != null)
                {
                    //add player to game Players list
                    game.PlayersLists.Add(pList);
                    game.AddPlayer(player);
                }
                db.SaveChanges();
                return RedirectToAction("Details/" + id);
            }
            return View(game);
        }

        // GET: Games/Details/5
        public ActionResult HostDetails(int? id)
        {
            int gameID = id ?? -1;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //the game that matched the id
            Game game = db.Games.Find(gameID);

            //current player id
            string currentp = User.Identity.GetUserId();

            // player element of current player id
            Player player = db.Players.Where(x => x.AuthUserID.Equals(currentp)).FirstOrDefault();

            ViewBag.playerID = player.PlayerID;

            if (game != null && player.AuthUserID.Equals(currentp) && game.Host == player.PlayerID)
            {
                ViewBag.Player = "Host";
            }
            else
            {
                return HttpNotFound();
            }

            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // POST: Games/Details
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HostDetails([Bind(Include = "GameID, PlayerID")]  int id)
        {

            string tempPlayer = User.Identity.GetUserId();
            //make Player and set it to current user
            Player player = db.Players.Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();

            Game game = db.Games.FirstOrDefault(s => s.GameID == id);
            if (ModelState.IsValid)
            {
                if (game != null)
                {
                    game.AddPlayer(player);
                }
                db.SaveChanges();
                return RedirectToAction("Details/" + id);
            }
            return View(game);
        }

        // GET: Games/Create
        public ActionResult Create()
        {
            ViewBag.Host = new SelectList(db.Players, "PlayerID", "FirstName");
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GameID,Name,Host,Active,StartDate,EndDate,Location,LocationCenterLong,LocatoinCenterLat,LocationRadius")] Game game)
        {
            //get playerID so host can be set
            string tempPlayer = User.Identity.GetUserId();
            Player player = db.Players.Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();

            if (ModelState.IsValid)
            {
                game.Host = player.PlayerID;
                db.Games.Add(game);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Host = new SelectList(db.Players, "PlayerID", "FirstName", game.Host);
            return View(game);
        }

        // GET: Games/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }

            //prevents user editing other user's game
            string tempPlayer = User.Identity.GetUserId();
            Player player = db.Players.Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();

            if (player.PlayerID != game.Host)
            {
                return View("Error");
            }
            /////

            ViewBag.Host = new SelectList(db.Players, "PlayerID", "FirstName", game.Host);
            return View(game);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GameID,Name,Host,Active,StartDate,EndDate,Location,LocationCenterLong,LocatoinCenterLat,LocationRadius")] Game game)
        {
            string tempPlayer = User.Identity.GetUserId();
            Player player = db.Players.Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();
            if (ModelState.IsValid)
            {
                game.Host = player.PlayerID;
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Dashboard");
            }
            ViewBag.Host = new SelectList(db.Players, "PlayerID", "FirstName", game.Host);
            return View(game);
        }

        // GET: Games/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            //prevents user deleting other user's game
            string tempPlayer = User.Identity.GetUserId();
            Player player = db.Players.Where(x => x.AuthUserID.Equals(tempPlayer)).FirstOrDefault();

            if (player.PlayerID != game.Host)
            {
                return View("Error");
            }
            /////

            return View(game);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            Game game = db.Games.Find(id);

            //remove all players form game before deleting it
            while (game.Players.Count() != 0)
            {
                Player v = game.Players.First();
                game.Players.Remove(v);
            };

            db.Games.Remove(game);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpGet]
        public ActionResult AddPlayers(int? id)
        {
            if (!ModelState.IsValid)
            {
                return View("Login");
            }

            int gameID = id ?? -1;
            Game game = db.Games.Where(g => g.GameID == id).FirstOrDefault();

            Debug.WriteLine($"The game is {game.Name}");

            GamePlayers gamePlayers = new GamePlayers();
            gamePlayers.Game = game;
            gamePlayers.Host = db.Players.Where(p => p.PlayerID == game.Host).FirstOrDefault();
            gamePlayers.Players = game.Players;
            gamePlayers.AllPlayers = db.Players;

            ViewBag.Game = game.GameID;

            string names = "";

            foreach (var player in gamePlayers.Players)
            {
                names += $" '{player.FirstName} {player.LastName}',";
            }

            if (names.Count() > 0)
                names = names.Remove(names.Count() - 1, 1);

            Debug.WriteLine($"Names are: {names}");
            ViewBag.Names = names;

            return View("InvitePlayers", gamePlayers);
        }

        /// <summary>
        /// Updates the PlayersList in the database for a particular game and returns
        /// a Json object of new players to the InvitePlayers view.
        /// </summary>
        /// <param name="ids">The id's of the players to add</param>
        /// <param name="gid">The gameID of the game to add the players to</param>
        /// <returns>A Json object of players.</returns>
        [HttpPost]
        public JsonResult AddPlayers(List<string> ids, int? gid, int? currPID)
        {
            if (ids == null)
                ids = new List<string>();

            int gameID = gid ?? -1;
            Game game = db.Games.Where(g => g.GameID == gameID).FirstOrDefault();

            string authUser = User.Identity.GetUserId();

            Player currPlayer = db.Players.Where(cp => cp.AuthUserID.Equals(authUser)).FirstOrDefault();

            List<Player> players = db.Players.Where(p => ids.Contains(p.PlayerID.ToString())).ToList();

            //Need to create a simple model of players because Json has issues with serializing 
            //Entity Framework Models
            List<JsonGamePlayers> gPees = new List<JsonGamePlayers>();

            if (game.Host == currPlayer.PlayerID)
            {
                List<PlayersList> games = game.PlayersLists.Where(pl => pl.GameID == game.GameID).ToList();

                //Remove all players from the game
                foreach (var oldPlayer in games)
                {
                    game.RemovePlayer(oldPlayer.Player.PlayerID);
                    oldPlayer.Player.PlayersLists.Remove(oldPlayer);
                    //oldPlayer.RemovePlayerFromGame(oldPlayer.Player, oldPlayer.Game);
                    db.Entry(oldPlayer).State = EntityState.Modified;
                }

                game.PlayersLists.Clear();
                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();

                //Add selected players to the database
                foreach (var newPlayer in players)
                {
                    game.AddPlayer(newPlayer);
                    db.Entry(newPlayer).State = EntityState.Modified;
                }

                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();

                PlayersList plists = db.PlayersLists.Where(pl => pl.GameID == game.GameID).FirstOrDefault();

                Random rnd = new Random();

                //add random 4 digit integer for targetCode for each player.
                foreach (var p in players)
                {
                    PlayersList playersList = new PlayersList();
                    playersList.AddEntry(p, game, rnd.Next(1, 9999));
                    
                    db.PlayersLists.Add(playersList);

                    db.Entry(p).State = EntityState.Modified;
                }

                db.Entry(game).State = EntityState.Modified;
                db.SaveChanges();

                foreach (var player in players)
                {
                    JsonGamePlayers gp = new JsonGamePlayers()
                    {
                        PlayerID = player.PlayerID,
                        UserName = player.UserName,
                        Email = player.Email,
                        Phone = player.Phone,
                        PhotoUrl = player.PhotoUrl
                    };
                    gPees.Add(gp);
                }
            }
            else
            {
                return Json("Account/Login", JsonRequestBehavior.AllowGet);
            }

            return Json(gPees, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> InvitePlayers(int? gameID, string sendType)
        {
            if (gameID == null)
                gameID = -1;

            var game = db.Games.Where(g => g.GameID == gameID).FirstOrDefault();
            List<string> ids = new List<string>();

            foreach (var player in game.Players)
            {
                ids.Add(player.PlayerID.ToString());
            }

            switch (sendType)
            {
                case "email" :
                    return await SendEmails(ids, game);
                case "sms":
                    return await SendSMS(ids);
            }

            return Json("Error", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> SendEmails(List<string> ids, Game game)
        {
            if (ids == null)
                ids = new List<string>();

            var players = db.Players.Where(p => ids.Contains(p.PlayerID.ToString())).ToList();

            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            // setup Smtp authentication
            NetworkCredential credentials =
                new NetworkCredential("eliminationframework@gmail.com", "Elimination@21");
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;
            //can be obtained from your model
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("eliminationframework@gmail.com");

            foreach (var player in players)
            {
                msg.To.Clear();
                msg.To.Add(player.Email);

                msg.Subject = $"From The Elimination Framework";
                msg.IsBodyHtml = true;

                var target = game.Targets.Where(g => g.APlayer.PlayerID == player.PlayerID).FirstOrDefault().TargetPlayer;

                msg.Body = string.Format($@"
                    <html>
                        <head>
                           <b><H1>Target Assgnment:</h1><b>
                           <H3>Name: {target.FirstName} {target.LastName}.</h3>
                        </head>
                        <body>
                           <b>Assignment Date: {DateTime.Now}</b>
                           <h3>Your target is:</h3> <img src =""{target.PhotoUrl}""/>
                        </body>
                    </html>");
                try
                {
                    await client.SendMailAsync(msg);
                }
                catch (Exception ex)
                {
                    return Json("error:" + ex.ToString(), JsonRequestBehavior.AllowGet);
                }
            }
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SendSMS(List<string> playerIDs)
        {
            List<Player> players = db.Players.Where(p => playerIDs.Contains(p.PlayerID.ToString())).ToList();

            SMSNotification notification = new SMSNotification(players);
            await notification.SendMessagesAsync("Hello from the Elimination Framework");

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Error()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ViewPlayer()
        {
            return View(db.Players.Where(p => p.PlayerID == 15).FirstOrDefault());
        }

        public ActionResult EliminatePlayer(int gameID, int targetID, string targetCode, string redirect = "HostDetails")
        {
            Player eliminatedPlayer = db.Players.Find(targetID);
            PlayersList pl = eliminatedPlayer.PlayersLists.Where(g => g.GameID == gameID).FirstOrDefault();
            string ep = pl.TargetCode.ToString();

            if (targetCode == ep)
            {
                Elimination.Models.Elimination elimin = db.Games.FirstOrDefault(g => g.GameID == gameID).EliminatePlayer(targetID);
                db.Eliminations.Add(elimin);
                db.SaveChanges();
                return RedirectToAction(redirect, new { id = gameID, eliminationConfirmation = "Target Eliminated!" });

            }
            else
            {
                return RedirectToAction(redirect, new { id = gameID, eliminationConfirmation = "Wrong Code!" });
            }
        }

        public ActionResult HostGames()
        {
            var id = User.Identity.GetUserId();
            int playerId = -1;
            if (id != null)
                playerId = db.Players.Where(p => p.AuthUserID == id).FirstOrDefault().PlayerID;
            return View(db.Games.Where(g => g.Player.PlayerID == playerId));
        }

        public ActionResult ImageElimination(int gameId, int targetId)
        {
            ViewBag.GameID = gameId;
            return View(db.Players.FirstOrDefault(p => p.PlayerID == targetId));
        }

        public async Task<JsonResult> EliminatePlayerAJAX(int gameID, int playerID, byte[] img)
        {
            using (var db = new EliminationDBContext())
            {
                var url = db.Players.FirstOrDefault(p => p.PlayerID == playerID).PhotoUrl;

                var sf = await FacialRecognition.Compare(img, url);
                if (sf.Confidence > 0.75)
                {
                    var game = db.Games.FirstOrDefault(g => g.GameID == gameID);
                    game.EliminatePlayer(playerID);
                    db.Entry(game).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { success = true, confidence = sf.Confidence });
                }
                else
                {
                    return Json(new { success = false, confidence = sf.Confidence });
                }
            }
        }

        public async Task<dynamic> IsFaceAJAX(byte[] img)
        {
            string isFace = await FacialRecognition.IsFace(img);
            return Json(isFace);
        }
    }

}
